﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace WCFChat
{
    [DataContract]
    public class Message
    {
        private User _user;
        private string _text;
        private DateTime _date;

        [DataMember]
        public DateTime Date { get => _date; set => _date = value; }
        [DataMember]
        public string Text { get => _text; set => _text = value; }
        [DataMember]
        public User User { get => _user; set => _user = value; }

        public Message(User user, string text, DateTime date)
        {
            _user = user;
            _text = text;
            _date = date;
        }



        public override string ToString()
        {
            return $"[{Date.ToString("yyyy-MM-dd HH:mm")}] {_user.Nick}: {Text}";
        }
    }
}
