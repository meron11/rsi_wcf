﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace WCFChat
{
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
    public class Server : IServer
    {
        private Messages _messages;

        public Server()
        {
            _messages = new Messages();
        }

        public void ReciveMessage(Message msg)
        {
            _messages.Add(msg);
        }

        public Messages GetMessages()
        {
            return _messages;
        }
    }
}
