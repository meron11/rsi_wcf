﻿using System.ServiceModel;

namespace WCFChat
{
    [ServiceContract]
    public interface IServer
    {
        [OperationContract]
        void ReciveMessage(Message msg);

        [OperationContract]
        Messages GetMessages();
    }
}