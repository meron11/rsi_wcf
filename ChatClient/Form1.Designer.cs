﻿namespace ChatClient
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.Connect = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.nick = new System.Windows.Forms.TextBox();
            this.Connection = new System.Windows.Forms.GroupBox();
            this.poolMessages = new System.Windows.Forms.Timer(this.components);
            this.messages = new System.Windows.Forms.RichTextBox();
            this.send = new System.Windows.Forms.Button();
            this.message = new System.Windows.Forms.TextBox();
            this.Connection.SuspendLayout();
            this.SuspendLayout();
            // 
            // Connect
            // 
            this.Connect.Location = new System.Drawing.Point(700, 21);
            this.Connect.Name = "Connect";
            this.Connect.Size = new System.Drawing.Size(75, 23);
            this.Connect.TabIndex = 0;
            this.Connect.Text = "Connect";
            this.Connect.UseVisualStyleBackColor = true;
            this.Connect.Click += new System.EventHandler(this.Connect_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(25, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nickname:";
            // 
            // nick
            // 
            this.nick.Location = new System.Drawing.Point(89, 36);
            this.nick.Name = "nick";
            this.nick.Size = new System.Drawing.Size(108, 20);
            this.nick.TabIndex = 2;
            // 
            // Connection
            // 
            this.Connection.Controls.Add(this.Connect);
            this.Connection.Location = new System.Drawing.Point(13, 13);
            this.Connection.Name = "Connection";
            this.Connection.Size = new System.Drawing.Size(786, 62);
            this.Connection.TabIndex = 3;
            this.Connection.TabStop = false;
            this.Connection.Text = "Connection";
            // 
            // poolMessages
            // 
            this.poolMessages.Interval = 50;
            this.poolMessages.Tick += new System.EventHandler(this.poolMessages_Tick);
            // 
            // messages
            // 
            this.messages.Location = new System.Drawing.Point(13, 81);
            this.messages.Name = "messages";
            this.messages.ReadOnly = true;
            this.messages.Size = new System.Drawing.Size(786, 321);
            this.messages.TabIndex = 4;
            this.messages.Text = "";
            // 
            // send
            // 
            this.send.Location = new System.Drawing.Point(713, 415);
            this.send.Name = "send";
            this.send.Size = new System.Drawing.Size(75, 23);
            this.send.TabIndex = 5;
            this.send.Text = "Send";
            this.send.UseVisualStyleBackColor = true;
            this.send.Click += new System.EventHandler(this.send_Click);
            // 
            // message
            // 
            this.message.Location = new System.Drawing.Point(12, 415);
            this.message.Name = "message";
            this.message.Size = new System.Drawing.Size(695, 20);
            this.message.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(809, 450);
            this.Controls.Add(this.message);
            this.Controls.Add(this.send);
            this.Controls.Add(this.messages);
            this.Controls.Add(this.nick);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Connection);
            this.Name = "Form1";
            this.Text = "WCF Chat Client";
            this.Connection.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Connect;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox nick;
        private System.Windows.Forms.GroupBox Connection;
        private System.Windows.Forms.Timer poolMessages;
        private System.Windows.Forms.RichTextBox messages;
        private System.Windows.Forms.Button send;
        private System.Windows.Forms.TextBox message;
    }
}

