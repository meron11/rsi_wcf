﻿using ChatClient.ChatServer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatClient
{
    public partial class Form1 : Form
    {
        private ServerClient _sc;
        public Form1()
        {
            _sc = new ServerClient();
            InitializeComponent();
        }

        private void Connect_Click(object sender, EventArgs e)
        {
            if(String.IsNullOrEmpty(nick.Text))
            {
                MessageBox.Show("Nick can't be empty!");
                return;
            }
            poolMessages.Start();
            
        }

        private void poolMessages_Tick(object sender, EventArgs e)
        {

            try { 
                var msgs  = _sc.GetMessages();
                var buff = new StringBuilder();
                foreach(var msg in msgs)
                {
                    buff.AppendLine($"[{msg.Date.ToString("yyyy-MM-dd HH:mm")}] {msg.User.Nick}: {msg.Text}");
                }
                messages.Text = buff.ToString();
                messages.ScrollToCaret();
            } catch(Exception)
            {
                MessageBox.Show("Error has occured, connect again!");
                ((Timer)sender).Stop();
            }
        }

        private void send_Click(object sender, EventArgs e)
        {           
            var msg = new ChatServer.Message
            {
                Date = DateTime.Now,
                Text = message.Text,
                User = new User { Nick = nick.Text }
            };

            _sc.ReciveMessage(msg);
        }
    }
}
