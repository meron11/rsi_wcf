﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;
using WCFChat;

namespace ChatServer
{
    class Program
    {
        static void Main(string[] args)
        {
            Uri address = new Uri("http://localhost:8000/Chat/");
            ServiceHost host = new ServiceHost(typeof(Server), address);

            try
            {
                // Step 3 Add a service endpoint.  
                host.AddServiceEndpoint(typeof(IServer), new WSHttpBinding(), "ChatService");
                //// Step 4 Enable metadata exchange.  
                ServiceMetadataBehavior smb = new ServiceMetadataBehavior();
                smb.HttpGetEnabled = true;
                host.Description.Behaviors.Add(smb);

                // Step 5 Start the service.  
                host.Open();
                Console.WriteLine("The chat server is ready.");
                Console.WriteLine("Press <ENTER> to terminate service.");
                Console.WriteLine();
                Console.ReadLine();
                host.Close();
            }
            catch (CommunicationException ce)
            {
                Console.WriteLine("An exception occurred: {0}", ce.Message);
                host.Abort();
            }
        }
    }
}
